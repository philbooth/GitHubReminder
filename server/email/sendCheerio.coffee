'use strict'

log = require '../log'

initialise = (transport, config, subjectPrefix) ->
  log = log.initialise 'email/goodbye'
  log.info 'initialising'

  (event) ->
    { to, user, frequency, repo, paths } = event.getData()

    text = """
           Hey #{user},


           This is your final email from GitHubReminder. My maker is turning me off and deleting my database. I will soon be dead.

           My dying wish is to send one last reminder email that you may remember me by. I hope it's a belter.

           Your starred repo of the death is called #{repo.name}.


           Repository: #{repo.full_name}
           Description: #{repo.description || ''}
           Language: #{repo.language || ''}
           Stars: #{repo.watchers_count}
           Forks: #{repo.forks_count}

           #{repo.html_url}

           Cheerio,
           the reminder bot.
           """

    log.info "sending email to #{to}:"
    console.log text

    transport.sendMail {
      from: config.from
      to
      subject: "#{subjectPrefix} Cheerio"
      text
    }, event.respond

module.exports = { initialise }

